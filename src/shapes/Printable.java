package shapes;

public interface Printable {
  String print();
}
