package shapes;

import java.util.List;

public class Rectangle implements Shape {

  private List<Point> pointList;

  public Rectangle(List<Point> pointList) {
    this.pointList = pointList;
  }

  public List<Point> getPointList() {
    return pointList;
  }

  @Override
  public double calculateArea() {
    return pointList.get(0).getDistance(pointList.get(1)) * pointList.get(1).getDistance(pointList.get(2));
  }

  @Override
  public double calculatePerimeter() {
    return pointList.get(0).getDistance(pointList.get(1)) +
        pointList.get(1).getDistance(pointList.get(2)) +
        pointList.get(2).getDistance(pointList.get(3)) +
        pointList.get(3).getDistance(pointList.get(0));
  }

  @Override
  public String print() {
    return String.format("Rectangle; Sides: %.3f, %.3f, %.3f, %.3f;  Area: %.3f; Perimeter: %.3f\n",
                         pointList.get(0).getDistance(pointList.get(1)),
                         pointList.get(1).getDistance(pointList.get(2)),
                         pointList.get(2).getDistance(pointList.get(3)),
                         pointList.get(3).getDistance(pointList.get(0)),
                         calculateArea(),
                         calculatePerimeter());
  }
}