package shapes;

import java.util.LinkedList;
import java.util.List;

public class Parallelogram extends Rectangle implements Shape {

  public Parallelogram(List<Point> pointList) {
    super(pointList);
  }

  @Override
  public double calculateArea() {
    List<Point> trianglePointList = new LinkedList<>();
    trianglePointList.add(getPointList().get(0));
    trianglePointList.add(getPointList().get(1));
    trianglePointList.add(getPointList().get(2));
    Triangle halfParallelogram = new Triangle(trianglePointList);
    return halfParallelogram.calculateArea() * 2;
  }


  @Override
  public String print() {
    return String.format("Parallelogram; Sides: %.3f, %.3f, %.3f, %.3f;  Area: %.3f; Perimeter: %.3f\n",
                         getPointList().get(0).getDistance(getPointList().get(1)),
                         getPointList().get(1).getDistance(getPointList().get(2)),
                         getPointList().get(2).getDistance(getPointList().get(3)),
                         getPointList().get(3).getDistance(getPointList().get(0)),
                         calculateArea(),
                         calculatePerimeter());
  }
}
