package shapes;

import java.util.List;

public class Triangle implements Shape {
  private List<Point> pointList;

  public Triangle(List<Point> pointList) {
    this.pointList = pointList;
  }

  public List<Point> getPointList() {
    return pointList;
  }

  private Double calculateSemiPerimeter() {
    return calculatePerimeter() / 2;
  }

  @Override
  public double calculateArea() {
    return Math.sqrt(calculateSemiPerimeter() *
                         (calculateSemiPerimeter() - pointList.get(0).getDistance(pointList.get(1))) *
                         (calculateSemiPerimeter() - pointList.get(1).getDistance(pointList.get(2))) *
                         (calculateSemiPerimeter() - pointList.get(2).getDistance(pointList.get(0))));
  }

  @Override
  public double calculatePerimeter() {
    return pointList.get(0).getDistance(pointList.get(1)) +
        pointList.get(1).getDistance(pointList.get(2)) +
        pointList.get(2).getDistance(pointList.get(0));
  }

  @Override
  public String print() {
    return String.format("Triangle; Sides %.3f, %.3f, %.3f;  Area: %.3f; Perimeter: %.3f\n",
                         pointList.get(0).getDistance(pointList.get(1)),
                         pointList.get(1).getDistance(pointList.get(2)),
                         pointList.get(2).getDistance(pointList.get(0)),
                         calculateArea(),
                         calculatePerimeter());
  }
}
