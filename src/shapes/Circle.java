package shapes;

public class Circle implements Shape {
  private Point centerPoint;
  private Point circlePoint;

  public Circle(Point center, Point circlePoint) {
    this.centerPoint = center;
    this.circlePoint = circlePoint;
  }

  public Point getCenterPoint() {
    return centerPoint;
  }

  public void setCenterPoint(Point centerPoint) {
    this.centerPoint = centerPoint;
  }

  public Point getCirclePoint() {
    return circlePoint;
  }

  public void setCirclePoint(Point circlePoint) {
    this.circlePoint = circlePoint;
  }

  @Override
  public double calculateArea() {
    return Math.PI * Math.pow(calculateRadiusLength(), 2);
  }

  @Override
  public double calculatePerimeter() {
    return 2 * Math.PI * calculateRadiusLength();
  }

  @Override
  public String print() {
    return String.format("Circle, area: %.3f, perimeter: %.3f\n", calculateArea(), calculatePerimeter());
  }

  private double calculateRadiusLength() {
    return centerPoint.getDistance(circlePoint);
  }
}
