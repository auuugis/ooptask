package shapes;

public interface Shape extends Printable {
  double calculateArea();

  double calculatePerimeter();
}
