package shapes;


public class Point {
  private double coordinateX;
  private double coordinateY;

  public Point(double coordinateX, double coordinateY) {
    this.coordinateX = coordinateX;
    this.coordinateY = coordinateY;
  }

  public double getCoordinateX() {
    return coordinateX;
  }

  public double getCoordinateY() {
    return coordinateY;
  }

  public double getDistance(Point point) {
    return Math.sqrt(Math.pow(this.getCoordinateX() - point.getCoordinateX(), 2) +
                         Math.pow(this.getCoordinateY() - point.getCoordinateY(), 2));
  }

  @Override
  public String toString() {
    return "Point{" +
        "coordinateX=" + coordinateX +
        ", coordinateY=" + coordinateY +
        '}';
  }
}
