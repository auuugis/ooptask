package utils;

import shapes.*;

import java.util.LinkedList;
import java.util.List;

public class ShapeFactory {

  public Shape getShape(String line) {
    String[] splitedLine = line.split(" ");
    String shapeType = splitedLine[0];
    if ("C".equalsIgnoreCase(shapeType)) {
      Point centerPoint = new Point(Double.parseDouble(splitedLine[1]), Double.parseDouble(splitedLine[2]));
      Point circlePoint = new Point(Double.parseDouble(splitedLine[3]), Double.parseDouble(splitedLine[4]));
      return new Circle(centerPoint, circlePoint);
    }
    else if ("R".equalsIgnoreCase(shapeType)) {
      List<Point> pointList = new LinkedList<>();
      pointList.add(new Point(Double.parseDouble(splitedLine[1]), Double.parseDouble(splitedLine[2])));
      pointList.add(new Point(Double.parseDouble(splitedLine[3]), Double.parseDouble(splitedLine[4])));
      pointList.add(new Point(Double.parseDouble(splitedLine[5]), Double.parseDouble(splitedLine[6])));
      pointList.add(new Point(Double.parseDouble(splitedLine[7]), Double.parseDouble(splitedLine[8])));
      return new Rectangle(pointList);
    }
    else if ("T".equalsIgnoreCase(shapeType)) {
      List<Point> pointList = new LinkedList<>();
      pointList.add(new Point(Double.parseDouble(splitedLine[1]), Double.parseDouble(splitedLine[2])));
      pointList.add(new Point(Double.parseDouble(splitedLine[3]), Double.parseDouble(splitedLine[4])));
      pointList.add(new Point(Double.parseDouble(splitedLine[5]), Double.parseDouble(splitedLine[6])));
      return new Triangle(pointList);
    }
    else if ("P".equalsIgnoreCase(shapeType)) {
      List<Point> pointList = new LinkedList<>();
      pointList.add(new Point(Double.parseDouble(splitedLine[1]), Double.parseDouble(splitedLine[2])));
      pointList.add(new Point(Double.parseDouble(splitedLine[3]), Double.parseDouble(splitedLine[4])));
      pointList.add(new Point(Double.parseDouble(splitedLine[5]), Double.parseDouble(splitedLine[6])));
      pointList.add(new Point(Double.parseDouble(splitedLine[7]), Double.parseDouble(splitedLine[8])));
      return new Parallelogram(pointList);
    }

    return null;
  }
}
